package Task2;

public class AppMain {

    public static void main(String args [])
    {
        System.out.println("********* REQUEST INITIATED, PLEASE WAIT FOR RESPONSE *************************");
        System.out.println("FUNCTION 1 OUTPUT :>> "+ AppFunc.getUsernames(2));
        System.out.println("FUNCTION 2 OUTPUT :>> "+ AppFunc.getUsernameWithHighestCommentCount(1));
        System.out.println("FUNCTION 3 OUTPUT :>> "+ AppFunc.getUsernamesSortedByRecordDate(1));
    }
}