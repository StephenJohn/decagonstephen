package Task2;

import java.util.List;

import Task2.domain.Data;
import Task2.domain.ResponseModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

public class AppFunc {

    private static final String endpoint = "https://jsonmock.hackerrank.com/api/article_users/search?page=";

    // FUNCTION 1
    //The list of most active authors according to a set threshold
    public static List<String> getUsernames(int threshold) {
            List<Data> lstData = getData(threshold);
             return lstData.parallelStream()
                      .filter(d->d.getSubmitted()>0)
                      .map(data->data.getUsername())
                      .collect(Collectors.toList());
    }

    // FUNCTION 2
    // The author with the highest comment count.
    public static String getUsernameWithHighestCommentCount(int threshold)
    {
        List<Data> lstData = getData(threshold);
        Data data = lstData.parallelStream()
                .sorted(Comparator.comparing(Data::getCommentCount).reversed())
                .findFirst()
                .get();
      return data.getUsername();
    }


    // FUNCTION 3
    // The list of the authors sorted by when their record was created according to a set threshold.
    public static List<String> getUsernamesSortedByRecordDate(int threshold) {
        List<Data> lstData = getData(threshold);
             return  lstData.parallelStream()
                         .sorted(Comparator.comparing(Data::getUpdatedAt))
                         .map(data->data.getUsername())
                         .collect(Collectors.toList());
    }

   // Logic to Make API Call to Fetch Data from ThirdPirty Resource
    private static List<Data> getData(int threshold)
    {
        AppUtil.RestClient(endpoint+threshold);
        String response = AppUtil.RestClient(endpoint);
        Gson gson = new GsonBuilder().create();
        ResponseModel responseModel = gson.fromJson(response, ResponseModel.class);
        return responseModel.getData();
    }
}
