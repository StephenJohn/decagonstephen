package Task2;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class AppUtil {
        public static String  RestClient (String endpoint){
            URL url = null;
            String response="";

            try {
                url = new URL(endpoint);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Accept", "application/json");

                if (connection.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP error code : " + connection.getResponseCode());
                }

                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (connection.getInputStream())));

                String output;
                while ((output = br.readLine()) != null) {
                    response= response+output;
                }
                connection.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }


    }
