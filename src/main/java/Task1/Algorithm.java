package Task1;

public class Algorithm {

    int maximumNumberOfPairOfSocks(int noOfWashes, int []cleanPile, int []dirtyPile) {

        int maxIndex = 50;

        int [] dirtySocks = new int [maxIndex];
        int [] cleanSocks = new int [maxIndex];
        int  numberOfPairOfSocks = 0;


        for(int d : dirtyPile) {
            dirtySocks[d]++;
        }

        for(int c : cleanPile) {
            cleanSocks[c]++;
        }


        for(int counter= 0; counter<maxIndex; counter++)
        {
            numberOfPairOfSocks =numberOfPairOfSocks+ (cleanSocks[counter]/2);

            // If it is not paired AND noOfWashes Not Exausted AND dirtySocks is Still left,
            // Reduce no of Washing by 1
            // Reduce the number of Dirty Socks by 1
            // Increment the Maximum Number Of Pair Socks
            if((cleanSocks[counter]%2!=0) && (noOfWashes>0) && (dirtySocks[counter]>0))
            {
                noOfWashes--;
                dirtySocks[counter]--;
                numberOfPairOfSocks++;
            }
        }



        for(int counter=0; (noOfWashes>1 && counter<maxIndex); counter++)
        {
            if(dirtySocks[counter] >=2)
            {
                dirtySocks[counter] =Math.min(dirtySocks[counter]/2, noOfWashes/2);
                numberOfPairOfSocks =numberOfPairOfSocks+ dirtySocks[counter];
                noOfWashes -= dirtySocks[counter]*2;
            }
        }
        return numberOfPairOfSocks;
    }
}
